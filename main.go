package main

import (
	"fmt"
	"os"

	asposepdfcloud "github.com/aspose-pdf-cloud/aspose-pdf-cloud-go/v22"
)

func getEnv(key string) string {
	env := os.Getenv(key)
	if env == "" {
		panic("no " + key)
	}
	return env
}

func main() {
	fname := getEnv("ASPOSE_APP_FNAME")
	f, err := os.Open(fname)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	pdfApi := asposepdfcloud.NewPdfApiService(getEnv("ASPOSE_APP_SID"), getEnv("ASPOSE_APP_KEY"), "")
	// https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-go/blob/master/docs/PdfApi.md#uploadfile
	_, res, err := pdfApi.UploadFile(fname, f, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("UploadFile: " + res.Status)
	// https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-go/blob/master/docs/PdfApi.md#getdocument
	dr, res, err := pdfApi.GetDocument(fname, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("GetDocument: " + res.Status)
	fmt.Println("Properties")
	for _, dp := range dr.Document.DocumentProperties.List {
		fmt.Printf("  %s: %s\n", dp.Name, dp.Value)
	}
	// https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-go/blob/master/docs/PdfApi.md#deletefile
	res, err = pdfApi.DeleteFile(fname, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("DeleteFile: " + res.Status)
}
