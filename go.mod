module gitlab.com/esopsa/aspose.pdf.cloud/gotest

go 1.16

require github.com/aspose-pdf-cloud/aspose-pdf-cloud-go/v22 v22.3.1
